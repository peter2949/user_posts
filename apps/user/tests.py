from django.contrib.auth.hashers import make_password
from rest_framework import status
from django.urls import reverse
from rest_framework.test import APITestCase
from model_bakery import baker

from user.models import User


class UserTest(APITestCase):
    """ Test sign-up, login, list, search users. """

    def setUp(self):
        """ Set data for testing. """
        self.user = baker.make(User, _quantity=10)
        self.user_2 = User.objects.create(username="John Brasilski", password=make_password("QwertyQwerty"), sex="m")

    def test_create_user(self):
        """ Ensure we get 201 response from API after create a new user. """
        data = {
            'username': 'testuser',
            'password': 'somepasswordqwe',
            'sex': 'm'
        }
        url = reverse('user:sign-up')
        response = self.client.post(url, data)
        self.assertEqual(User.objects.count(), 12)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_token(self):
        """ Ensure we get 200 response from API after login and get a token """
        data = {
            "username": 'John Brasilski',
            "password": 'QwertyQwerty'
        }
        url = reverse('user:api_token_auth')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)

    def test_create_user_with_short_password(self):
        """ Ensure user is not created for password lengths less than 8. """
        data = {
            'username': 'name',
            'password': 'short'
        }
        url = reverse('user:sign-up')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 11)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_no_password(self):
        """ Ensure we get 400 response from API after attempts to enter an blank password """
        data = {
            'username': 'fanatic',
            'password': ''
        }
        url = reverse('user:sign-up')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_preexisting_username(self):
        """ Ensure we get 400 response from API after attempts to enter an existing username """
        data = {
            'username': self.user_2.username,
            'password': 'testuseralready',
        }
        url = reverse('user:sign-up')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data['username']), 1)

    def test_users_list_and_pagination(self):
        """ Ensure we get 200 response from API after retrieve list users, 10 per page """
        url = reverse('user:list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 10)
        self.assertNotEqual(len(response.data['results']), 11)

    def test_search_users(self):
        """
        Ensure we get 200 response from API after successful search and getting result
        """
        url = f'{reverse("user:search")}?search=m'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

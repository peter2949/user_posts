from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from user import views

app_name = 'user'

urlpatterns = [
    path('login/', obtain_auth_token, name='api_token_auth'),
    path('sign-up/', views.SignUPUserAPIView.as_view(), name='sign-up'),
    path('list/', views.UserListAPIView.as_view(), name='list'),
    path('search/', views.SearchAPIView.as_view(), name='search'),
]

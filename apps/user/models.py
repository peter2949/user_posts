from decimal import Decimal
from django.contrib.auth.models import UserManager
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.contrib.postgres.fields import JSONField

USER_SEX = (('f', 'female'), ('m', 'male'))


class User(AbstractBaseUser, PermissionsMixin):
    """ User model - contains detail User information """

    USERNAME_FIELD = "username"
    username = models.CharField(max_length=30, unique=True)
    birthday = models.DateField(null=True, blank=True)
    bio = models.TextField(blank=True, null=True)
    sex = models.CharField(max_length=1, choices=USER_SEX, help_text="Female or male gender")
    location = JSONField(blank=True, null=True, help_text='example: {"latitude": "28.921631","longitude": "4.463427"}')
    rating = models.DecimalField(decimal_places=2, max_digits=100, default=Decimal(0))
    show_in_search_results = models.BooleanField(default=False, help_text="Show profile in search results")

    objects = UserManager()

    def __str__(self):
        return self.username

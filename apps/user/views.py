from rest_framework import filters
from rest_framework.generics import CreateAPIView, ListAPIView

from user import serializers
from user.models import User

__all__ = [
    "SignUPUserAPIView",
    "UserListAPIView",
    "SearchAPIView",
]


class SignUPUserAPIView(CreateAPIView):
    """ User registration """
    queryset = User.objects.all()
    serializer_class = serializers.CreateUserSerializer


class UserListAPIView(ListAPIView):
    """ Retrieve users list, per 10 of page """
    queryset = User.objects.all()
    serializer_class = serializers.UserListSearchSerializer


class SearchAPIView(ListAPIView):
    """ Retrieve users list after search, if true show in search results, per 10 of page """
    search_fields = ["location", "rating", "sex", "birthday"]
    filter_backends = (filters.SearchFilter,)
    queryset = User.objects.filter(show_in_search_results=True)
    serializer_class = serializers.UserListSearchSerializer

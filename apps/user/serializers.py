from rest_framework import serializers

from user.models import User


class CreateUserSerializer(serializers.ModelSerializer):
    """ Serializer to create a user"""
    password = serializers.CharField(max_length=128, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ["username", "password", "birthday", "bio", "rating", "sex", "location", "show_in_search_results"]
        read_only_field = ["id"]

    def save(self, **kwargs):
        user = User(**self.validated_data)
        password = self.validated_data["password"]
        user.set_password(password)
        user.save()


class UserListSearchSerializer(serializers.ModelSerializer):
    """ Serializer for search and retrieve users list """

    class Meta:
        model = User
        exclude = ["password", "show_in_search_results"]

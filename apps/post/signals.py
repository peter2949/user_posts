from django.db.models.signals import post_save
from django.dispatch import receiver

from post.models import PostHistory, Post


@receiver(post_save, sender=Post)
def post_history(sender, instance, **kwargs):
    """ Signal saves the history of post changes """
    PostHistory.objects.create(author=instance.author, text=instance.text, post=instance)

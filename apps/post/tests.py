from random import randint
from django.contrib.auth.hashers import make_password
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from rest_framework import status
from model_bakery import baker

from post.models import Post
from user.models import User


class PostTests(APITestCase):
    """ Test class for testing a Post model. create, viewing a single and delete post """

    def setUp(self):
        """ Set data for testing """
        self.post = baker.make(Post, _quantity=10)
        self.test_post = Post.objects.create(author_id=1, text='test text')

        self.user = User.objects.create(username="John Brasilski", password=make_password("QwertyQwerty"), sex="m")
        self.token = Token.objects.create(user=self.user)
        self.post_2 = Post.objects.create(author_id=self.user.id, text='long text')

    def test_create_post(self):
        """ Ensure we get 201 response from API after create a post successfully """
        url = reverse('post:create')
        data = {'text': 'star wars'}
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Post.objects.count(), 13)

    def test_fail_create_post(self):
        """ Ensure we get 401 response from API when unauthorized user try create post """
        url = reverse('post:create')
        data = {'text': 'star track'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_detail(self):
        """ Ensure we get 200 response from API after retrieve post detail """
        response = self.client.get(reverse('post:detail', kwargs={"pk": self.test_post.author_id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('text' in response.data)

    def test_fail_detail_post(self):
        """ Make sure we get 404 responses from the API after requesting a non-existent single post """
        response = self.client.get(reverse('post:detail', kwargs={'pk': self.test_post.author_id + randint(15, 100)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_post(self):
        """ Ensure we get 204 response from API after delete post """
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.delete(reverse('post:delete', kwargs={'pk': self.post_2.id}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_forbidden_fail_delete_post(self):
        """ Ensure we get 403 response from API after delete post """
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.delete(reverse('post:delete', kwargs={'pk': 3}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_post(self):
        """ Ensure we get 200 response from API after post text update """
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        url = reverse('post:edit', kwargs={'pk': self.post_2.id})
        data = {'text': 'new text'}
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['text'], 'new text')

    def test_unauthorized_fail_update_post(self):
        """ Ensure we get 401 response from API after post text update """
        url = reverse('post:edit', kwargs={'pk': self.post_2.id})
        data = {'text': 'UNAUTHORIZED user cant edit post'}
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_edit_history(self):
        """ Ensure we get 200 response from API after retrieve posts list """
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get(reverse('post:edit-history'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 12)
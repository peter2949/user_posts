from rest_framework import generics
from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated

from post.serializers import PostCreateSerializer, PostEditSerializer, PostHistorySerializer, PostDetailSerializer
from post.models import Post, PostHistory
from post.permissions import IsOwner

__all__ = [
    "PostCreateAPIView",
    "PostEditAPIView",
    "PostDeleteAPIView",
    "PostHistoryAPIView",
]


class PostCreateAPIView(CreateAPIView):
    """ View create post, only auth user can create post """
    permission_classes = [IsAuthenticated, ]
    queryset = Post.objects.all()
    serializer_class = PostCreateSerializer


class PostEditAPIView(UpdateAPIView):
    """ View for edit post, only auth owner can edit post """
    permission_classes = [IsAuthenticated, IsOwner, ]
    queryset = Post.objects.all()
    serializer_class = PostEditSerializer


class PostDetailAPIView(RetrieveAPIView):
    """ View retrieve detail post, any users can see detail """
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer


class PostDeleteAPIView(DestroyAPIView):
    """ View for delete post """
    permission_classes = [IsAuthenticated, IsOwner, ]
    queryset = Post.objects.all()


class PostHistoryAPIView(generics.ListAPIView):
    """ View for post change history, any users can see history """
    permission_classes = [AllowAny]
    queryset = PostHistory.objects.all()
    serializer_class = PostHistorySerializer

from django.contrib import admin

from post.models import Post, PostHistory

admin.site.register(Post)
admin.site.register(PostHistory)

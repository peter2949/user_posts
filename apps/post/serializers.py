from rest_framework import serializers

from post.models import Post, PostHistory


class PostCreateSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Post
        fields = ['author', 'text', 'created', 'updated']


class PostEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['text']
        read_only_fields = ['author', 'updated', 'created']


class PostDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['author', 'text', 'created', 'updated']
        read_only_fields = ['__all__']


class PostHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PostHistory
        fields = ['post', 'author', 'text', 'timestamp']
        read_only_fields = ['__all__']

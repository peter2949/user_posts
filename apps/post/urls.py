from django.urls import path

from post import views

app_name = 'post'

urlpatterns = [
    path('create/', views.PostCreateAPIView.as_view(), name='create'),
    path('edit/<int:pk>/', views.PostEditAPIView.as_view(), name='edit'),
    path('detail/<int:pk>/', views.PostDetailAPIView.as_view(), name='detail'),
    path('delete/<int:pk>/', views.PostDeleteAPIView.as_view(), name='delete'),
    path('edit-history/', views.PostHistoryAPIView.as_view(), name='edit-history'),
]

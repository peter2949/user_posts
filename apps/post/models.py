from django.db import models

from user.models import User


class Post(models.Model):
    """ Model contain information's about post """
    author = models.ForeignKey(User, related_name="post", on_delete=models.PROTECT)
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class PostHistory(models.Model):
    """ Model contains information about post changes """
    post = models.ForeignKey(Post, related_name="history", on_delete=models.CASCADE)
    author = models.ForeignKey(User, related_name="post_history", on_delete=models.PROTECT)
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

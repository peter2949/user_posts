## General info

Basic Features (API endpoints):
1. User can: signup, login, receive user posts list.
2. Post actions: create, edit, delete, see edit history.
3. Search by fields: location, rating, sex, birthday.

Include DB dump.

Used technology: Django, Django Rest Framework.
```
$ virtualenv -p python3 venv
$ source ./venv/bin/activate
$ pip install -r requirements.txt

Create PostgreSQL DB and User for DB:

$ sudo -u postgres createuser admin_db
$ sudo -u postgres createdb marketplace_db
$ sudo -u postgres psql
psql=# ALTER USER admin_db with encrypted password 'DB_PASSWORD';
psql=# ALTER ROLE admin_db SET client_encoding TO 'utf8';
psql=# ALTER ROLE admin_db SET timezone TO 'UTC';
psql=# GRANT ALL PRIVILEGES ON DATABASE test_db TO admin_db ;

Make migration and start server:

$ python manage.py migrate
$ python manage.py runserver
``` 
